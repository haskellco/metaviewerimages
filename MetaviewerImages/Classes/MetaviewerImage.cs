﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using MetaviewerDataLayer.DAL;
using MetaviewerDataLayer.DAL.EntityFramework;
using System.Data.Objects;
using System.Reflection;
using System.Collections;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Security.Cryptography;

namespace MetaviewerImages
{
    public class MetaviewerImage
    {
        #region Properties
        private string copyToPath;
        public string CopyToPath
        {
            get { return copyToPath; }
            set { copyToPath = value; }
        }

        private int MAX_PAGES = Convert.ToInt32(ConfigurationManager.AppSettings["MaxPages"]);
        #endregion

        #region Constructors
        public MetaviewerImage()
        {

        } 
        #endregion

        #region Public Methods  

        /// <summary>
        /// Will return a path to a multiple tiff or pdf file based on specified parameters.
        /// </summary>
        /// <param name="docId"></param>
        /// <param name="NumberOfPages"></param>
        /// <param name="pdf"></param>
        /// <returns></returns>
        public string GetImage(int docId, int NumberOfPages = 10, bool pdf = true)
        {
            DocumentPages daDocumentPages = new DocumentPages();
            List<Page> tifImages = new List<Page>();
            ArrayList filePaths = new ArrayList();

            try
            {
                tifImages = daDocumentPages.GetPagesById(docId);

                if (String.IsNullOrEmpty(CopyToPath))
                {
                    if (pdf)
                    {
                        CopyToPath = ConfigurationManager.AppSettings["MVImagesDirectory"].ToString() +
                            docId + "_" + NumberOfPages + ".pdf";
                    }
                    else
                    {
                        CopyToPath = ConfigurationManager.AppSettings["MVImagesDirectory"].ToString() +
                            docId + "_" + NumberOfPages + ".tif";
                    }
                }

                if (tifImages.Count > 0)
                {
                    tifImages = tifImages.OrderBy(r => r.PageNumber).ToList();

                    // validate number of pages
                    if (NumberOfPages == 0) NumberOfPages = MAX_PAGES; // Default and cannot be 0

                    int processed = 0;
                    foreach (var item in tifImages)
                    {
                        filePaths.Add(item.DOSPath.Trim() + "\\" + item.MagSubDir.Trim() + "\\" + item.SysFile.Trim());
                        processed++;

                        // check if reached max pages and if will need to let user know about it
                        if (processed == MAX_PAGES && tifImages.Count > MAX_PAGES)
                        {
                            filePaths.Add(ConfigurationManager.AppSettings["UserNoticeTiff"].ToString());
                            processed++;
                        }

                        if (processed == NumberOfPages || processed == MAX_PAGES + 1) break;
                    }

                    if (pdf) MergeTIFF2Pdf(filePaths, CopyToPath);  //Default at this time
                    else MergeTIFImageFiles(filePaths, CopyToPath);
                }
                else
                {
                    throw new Exception("Document not found: " + docId);
                }
            }
            catch (Exception e)
            {
                if (e.Message.StartsWith("Document not found")) CopyToPath = e.Message;
                else if (e.Message.StartsWith("Please provide")) CopyToPath = e.Message;
                else
                {
                    // Modified by G. Vera 03/24/2012 - Changed the way it handles exception so that the
                    // UI class can catch this exception and email the doc id related to error.
                    string msg = "Error on MetaviewerImage class:\n" + e.Message + "\n\n" + e.StackTrace;
                    throw new Exception(msg);
                    //Logger.Write(msg);
                    //CopyToPath = String.Empty;
                }
            }

            return CopyToPath;
        }


        /// <summary>
        /// Will return a path to a multiple tiff or pdf file based on specified parameters.
        /// </summary>
        /// <param name="docId"></param>
        /// <param name="NumberOfPages"></param>
        /// <param name="pdf"></param>
        /// <returns></returns>
        public string GetImage(List<int> docIdArr, int NumberOfPages = 10, bool pdf = true)
        {
            DocumentPages daDocumentPages = new DocumentPages();
            List<Page> tifImages = new List<Page>();
            ArrayList filePaths = new ArrayList();

            foreach (var docId in docIdArr)
            {
                try
                {
                    tifImages = daDocumentPages.GetPagesById(docId);

                    if (String.IsNullOrEmpty(CopyToPath))
                    {
                        if (pdf)
                        {
                            CopyToPath = ConfigurationManager.AppSettings["MVImagesDirectory"].ToString() +
                                docId + "_" + NumberOfPages + ".pdf";
                        }
                        else
                        {
                            CopyToPath = ConfigurationManager.AppSettings["MVImagesDirectory"].ToString() +
                                docId + "_" + NumberOfPages + ".tif";
                        }
                    }

                    if (tifImages.Count > 0)
                    {
                        tifImages = tifImages.OrderBy(r => r.PageNumber).ToList();

                        // validate number of pages
                        if (NumberOfPages == 0) NumberOfPages = MAX_PAGES; // Default and cannot be 0

                        int processed = 0;
                        foreach (var item in tifImages)
                        {
                            filePaths.Add(item.DOSPath.Trim() + "\\" + item.MagSubDir.Trim() + "\\" + item.SysFile.Trim());
                            processed++;

                            // check if reached max pages and if will need to let user know about it
                            if (processed == MAX_PAGES && tifImages.Count > MAX_PAGES)
                            {
                                filePaths.Add(ConfigurationManager.AppSettings["UserNoticeTiff"].ToString());
                                processed++;
                            }

                            if (processed == NumberOfPages || processed == MAX_PAGES + 1) break;
                        }

                        if (pdf) MergeTIFF2Pdf(filePaths, CopyToPath);  //Default at this time
                        else MergeTIFImageFiles(filePaths, CopyToPath);
                    }
                    else
                    {
                        throw new Exception("Document not found: " + docId);
                    }
                }
                catch (Exception e)
                {
                    if (e.Message.StartsWith("Document not found")) CopyToPath = e.Message;
                    else if (e.Message.StartsWith("Please provide")) CopyToPath = e.Message;
                    else
                    {
                        // Modified by G. Vera 03/24/2012 - Changed the way it handles exception so that the
                        // UI class can catch this exception and email the doc id related to error.
                        string msg = "Error on MetaviewerImage class:\n" + e.Message + "\n\n" + e.StackTrace;
                        throw new Exception(msg);
                        //Logger.Write(msg);
                        //CopyToPath = String.Empty;
                    }
                }
            }

            return CopyToPath;
        }
        #endregion

        #region Private Methods

        private void MergeTIFImageFiles(string[] filePaths, string copyTo)
        {
            TiffManager tManager = new TiffManager();
            tManager.JoinTiffImages(filePaths, copyTo, System.Drawing.Imaging.EncoderValue.CompressionCCITT4);
        }

        private void MergeTIFImageFiles(ArrayList filePaths, string copyTo)
        {
            TiffManager tManager = new TiffManager();
            tManager.JoinTiffImages(filePaths, copyTo, System.Drawing.Imaging.EncoderValue.CompressionCCITT4);
        }

        private string MergeTIFF2Pdf(ArrayList filePaths, string copyTo)
        {
            TiffManager tManager = new TiffManager();
            return tManager.JoinTiffImages2Pdf(filePaths, copyTo);
        }

        #region Not in Use
        /// <summary>
        /// This will retunr the Stream where Image file was saved to
        /// </summary>
        /// <param name="filePaths"></param>
        //private Stream MergeTIFImageFiles(ArrayList filePaths)
        //{
        //    TiffManager tManager = new TiffManager();
        //    return tManager.JoinTiffImages(filePaths, System.Drawing.Imaging.EncoderValue.CompressionCCITT4);
        //}  
        #endregion


        #endregion

    }
}

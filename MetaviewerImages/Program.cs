﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using MetaviewerDataLayer.DAL.EntityFramework;
using System.Diagnostics;
using System.Configuration;

namespace MetaviewerImages
{
    class Program
    {
        static void Main(string[] args)
        {
            VendorInvoices invoices = new VendorInvoices();

            string file = String.Empty;

            if (args.Count() > 0)
            {
                switch (args.Count())
                {
                    case 1:
                        file = invoices.GetInvoiceImage(args[0].Substring(1) as String, 10);
                        break;
                    case 2:
                        file = invoices.GetInvoiceImage(args[0].Substring(1), Int32.Parse(args[1].Substring(1)));
                        break;
                    default:
                        break;
                }
            }
            else
            {
                //TODO:  error display what is needed.
            }

            //C:\Invoices\MetaviewerInvoiceImages.exe \1025148 \8
            if (file != String.Empty)
            {
                Process p = new Process();
                p.StartInfo.FileName = ConfigurationManager.AppSettings["WindowsProcessFN"].ToString();
                p.StartInfo.Arguments = ConfigurationManager.AppSettings["WindowsProcessArg"].ToString() + file;
                p.Start();

            }
        }
    }
}

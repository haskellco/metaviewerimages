﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaviewerDataLayer.DAL
{
    internal static class SQLStrings
    {
        private static string getInvoiceImages = @"SELECT VI.[E1 DOC no], DOC.DocName, P.PageNumber, P.DosPath, P.MagSubDir, VI.[Vendor Number], " +
            "P.SysFile, DOC.TypeID FROM ((dbo.[Vendor Invoices] VI INNER JOIN dbo.[Document] DOC ON VI.DocID = DOC.DocID) "  +
            "INNER JOIN dbo.[Page] P ON DOC.DocID = P.DocID) WHERE VI.[E1 DOC no] IS NOT NULL AND VI.[E1 DOC no] = @DocNo "  +  
            "AND DOC.LastRoute <> 85 ORDER BY VI.[E1 DOC no], P.PageNumber ASC";
        internal static string GetInvoiceImages
        {
          get { return SQLStrings.getInvoiceImages; }
          set { SQLStrings.getInvoiceImages = value; }
        }


        private static string getInvoiceImagesByVendor = @"SELECT VI.[E1 DOC no], DOC.DocName, P.PageNumber, P.DosPath, P.MagSubDir, VI.[Vendor Number], " +
            "P.SysFile, DOC.TypeID FROM ((dbo.[Vendor Invoices] VI INNER JOIN dbo.[Document] DOC ON VI.DocID = DOC.DocID) " +
            "INNER JOIN dbo.[Page] P ON DOC.DocID = P.DocID) WHERE VI.[E1 DOC no] IS NOT NULL AND VI.[E1 DOC no] = @DocNo " +
            "AND VI.[Vendor Number] = @Vendor AND DOC.LastRoute <> 85 ORDER BY VI.[E1 DOC no], P.PageNumber ASC";
        internal static string GetInvoiceImagesByVendor
        {
          get { return SQLStrings.getInvoiceImagesByVendor; }
          set { SQLStrings.getInvoiceImagesByVendor = value; }
        }

        private static string visaStatementsDocID = @"select DocID from [Document Base Form] where [Employee Number] = @EmpNo and cast([Statement Date] as date) = @STDate and [Document Type] = 'Visa Statement'";
        internal static string GetVisaStatementsDocIDs
        {
            get { return SQLStrings.visaStatementsDocID; }
            set { SQLStrings.visaStatementsDocID = value; }
        }

        //private static string getInvoiceImagesByJobAndDate = "SELECT LTRIM(RTRIM(VI.[Vendor Name])) AS Vendor, LTRIM(RTRIM(VI.[Inv Number])) AS Invoice, " + 
        //    "LTRIM(RTRIM(VI.[Job Number])) AS Job, VI.[Invoice Date], P.DosPath, P.MagSubDir, P.SysFile, VI.[Invoice Amount] " +

        //    "FROM ((dbo.[Vendor Invoices] VI INNER JOIN dbo.[Document] DOC " +
        //           " ON VI.DocID = DOC.DocID) INNER JOIN dbo.[Page] P " +
        //           " ON DOC.DocID = P.DocID) " +
        //    " WHERE VI.[E1 DOC no] IS NOT NULL AND " +
        //    " VI.[Job Number] LIKE '%' + @jobNumber + '%' AND ( CONVERT(VARCHAR,CONVERT(DATETIME,VI.[Invoice Date],101),101) >= convert(char(10),@fromDate,101) AND CONVERT(VARCHAR,CONVERT(DATETIME,VI.[Invoice Date],101),101) <= convert(char(10),@toDate,101) )  AND " +
        //    " DOC.LastRoute <> 85 AND VI.[Invoice Amount] >= @thresholdAmount";

        //AND ( CONVERT(VARCHAR,CONVERT(DATETIME,VI.[Invoice Date],101),101) >= convert(char(10),@fromDate,101) AND CONVERT(VARCHAR,CONVERT(DATETIME,VI.[Invoice Date],101),101) <= convert(char(10),@toDate,101) ) 

        private static string getInvoiceImagesByJobAndDate = "SELECT LTRIM(RTRIM(VI.[Vendor Name])) AS Vendor, LTRIM(RTRIM(VI.[Inv Number])) AS Invoice, " +
            "LTRIM(RTRIM(VI.[Job Number])) AS Job, VI.[Invoice Date], P.DosPath, P.MagSubDir, P.SysFile, VI.[Invoice Amount] " +

            "FROM ((dbo.[Vendor Invoices] VI INNER JOIN dbo.[Document] DOC " +
                   " ON VI.DocID = DOC.DocID) INNER JOIN dbo.[Page] P " +
                   " ON DOC.DocID = P.DocID) " +
            " WHERE VI.[E1 DOC no] IS NOT NULL AND " +
            " VI.[Job Number] LIKE '%' + @jobNumber + '%' AND ( VI.[Invoice Date] BETWEEN @fromDate AND @toDate )  AND " +
            " DOC.LastRoute <> 85 AND VI.[Invoice Amount] >= @thresholdAmount";

        internal static string GetInvoiceImagesByJobAndDate
        {
            get { return SQLStrings.getInvoiceImagesByJobAndDate; }
            set { SQLStrings.getInvoiceImagesByJobAndDate = value; }
        }

        private static string getExpenseReportsByE1DocNoAndDate = "SELECT LTRIM(RTRIM(VI.[Vendor Name])) AS Vendor, LTRIM(RTRIM(VI.[Inv Number])) AS Invoice, " +
            "LTRIM(RTRIM(VI.[Job Number])) AS Job, VI.[Invoice Date], P.DosPath, P.MagSubDir, P.SysFile, VI.[Invoice Amount] " +

            "FROM ((dbo.[Vendor Invoices] VI INNER JOIN dbo.[Document] DOC " +
                   " ON VI.DocID = DOC.DocID) INNER JOIN dbo.[Page] P " +
                   " ON DOC.DocID = P.DocID) " +
            "WHERE VI.[E1 DOC no] IN (select * from string_split(@docNumbers,','))";

        internal static string GetExpenseReportsByE1DocNoAndDate
        {
            get { return SQLStrings.getExpenseReportsByE1DocNoAndDate; }
            set { SQLStrings.getExpenseReportsByE1DocNoAndDate = value; }
        }
    }
}

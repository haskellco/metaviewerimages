﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using System.Data.Metadata.Edm;
using MetaviewerDataLayer.DAL.EntityFramework;

namespace MetaviewerDataLayer.DAL
{
    public static class HaskellDBContextManager
    {
        private static readonly MetaviewerEntities context = new MetaviewerEntities();

        //public HaskellDBContextManager() { }

        public static MetaviewerEntities Context
        {
            get { return context; }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using MetaviewerDataLayer.DAL.EntityFramework;

namespace MetaviewerDataLayer.DAL
{
    public class DataAccess<TEntity> : IDataAccess<TEntity> where TEntity: EntityObject
    {
        private MetaviewerEntities haskellContext;
        public MetaviewerEntities HaskellContext
        {
            get {
                    if (haskellContext == null)
                    {
                        haskellContext = HaskellDBContextManager.Context;
                    }

                    return haskellContext;
                }
            set { haskellContext = value; }
        }

        private string entitySetName;
        public string EntitySetName
        {
            get
            {
                if (String.IsNullOrEmpty(entitySetName))
                {
                    entitySetName = HaskellContext.GetEntitySetName(typeof(TEntity).Name);
                }
                return entitySetName;
            }
            set { entitySetName = value; }
        }


        public DataAccess()
        {
            // TODO: Complete member initialization
        }


        #region IDataAccess<TEntity> Methods


        public TEntity Insert(TEntity entity)
        {
            HaskellContext.AddObject(EntitySetName, entity);
            //HaskellContext.CreateObject<TEntity>().AddObject(entity);
            CommitChanges();

            return entity;
        }

        public TEntity Update(TEntity entity)
        {
            HaskellContext.CreateObjectSet<TEntity>(EntitySetName).AddObject(entity);
            CommitChanges();

            return entity;
        }

        public void Delete(TEntity entity)
        {
            HaskellContext.DeleteObject(entity);
            CommitChanges();
        }

        public void CommitChanges()
        {
            HaskellContext.SaveChanges();
        }

        //public List<TEntity> GetData()
        //{
        //    return HaskellContext.CreateObjectSet<TEntity>(this.EntitySetName).ToList();
        //}


        #endregion

        #region Archive
        //public static class TransferContentTypeData
        //{
        //    public static List<TransferContentType> GetData()
        //    {
        //        return blTransferContentType.GetData();
        //    }

        //    public static List<TransferContentType> InsertData(TransferContentType newObject)
        //    {
        //        return blTransferContentType.Insert(newObject);
        //    }

        //    public static List<TransferContentType> UpdateData(TransferContentType updatedObject)
        //    {
        //        return blTransferContentType.Update(updatedObject);
        //    }

        //    public static List<TransferContentType> DeleteData(TransferContentType deleteObject)
        //    {
        //        return blTransferContentType.Update(deleteObject);
        //    }

        //    public static void CommitChanges()
        //    {
        //        blTransferContentType.CommitAllChanges();
        //    }

        //}
        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetaviewerDataLayer.DAL.EntityFramework;

namespace MetaviewerDataLayer.DAL
{
    public class DataFactory : IDataFactory
    {
        public IDataAccess<Page> GetPageDataAccess()
        {
            return new DataAccess<Page>();
        }
    }
}

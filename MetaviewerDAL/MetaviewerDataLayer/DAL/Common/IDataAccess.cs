﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaviewerDataLayer.DAL
{
    public interface IDataAccess<TEntity>
    {
        TEntity Insert(TEntity entity);
        TEntity Update(TEntity entity);
        //List<TEntity> GetData();
        void Delete(TEntity entity);
        void CommitChanges();
    }
}

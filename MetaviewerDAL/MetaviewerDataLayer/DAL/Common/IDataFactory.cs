﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetaviewerDataLayer.DAL.EntityFramework;

namespace MetaviewerDataLayer.DAL
{
    public interface IDataFactory
    {
        IDataAccess<Page> GetPageDataAccess();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using MetaviewerDataLayer.DAL.EntityFramework;
using System.Data.Objects;
using System.Configuration;
using System.Xml;
using System.IO;

namespace MetaviewerDataLayer.DAL.EntityFramework
{
    public class MVInvoicePages
    {
        public MetaviewerEntities context = new MetaviewerEntities();

        public MVInvoicePages()
        {

        }

        public List<GetDocumentsPage_Result> GetInvoicePages(string E1DocNo, int vendorNo = 0)
        {
            MetaviewerDataLayer.DAL.EntityFramework.MetaviewerEntities c = new MetaviewerEntities();

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["MVSQLConn"].ToString());
            List<GetDocumentsPage_Result> result = new List<GetDocumentsPage_Result>();
            GetDocumentsPage_Result resultObj;

            string query = SQLStrings.GetInvoiceImages;
            if (vendorNo != 0) query = SQLStrings.GetInvoiceImagesByVendor;

            connection.Open();

            try
            {
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.Add(new SqlParameter("DocNo", E1DocNo));
                if (vendorNo != 0)  command.Parameters.Add(new SqlParameter("Vendor", vendorNo));
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        resultObj = new GetDocumentsPage_Result();
                        resultObj.DocName = reader["DocName"] as String;
                        resultObj.DosPath = reader["DosPath"] as String;
                        resultObj.E1_DOC_no = reader["E1 DOC no"] as String;
                        resultObj.MagSubDir = reader["MagSubDir"] as String;
                        resultObj.PageNumber = (Int32)reader["PageNumber"];
                        resultObj.SysFile = reader["SysFile"] as String;
                        resultObj.TypeID = (Int32)reader["TypeID"];
                        resultObj.VendorNumber = (reader["Vendor Number"] != null) ? reader["Vendor Number"].ToString() : "0";

                        result.Add(resultObj);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return result;
        }
    }
}

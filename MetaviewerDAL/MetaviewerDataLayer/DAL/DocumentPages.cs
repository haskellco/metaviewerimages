﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using MetaviewerDataLayer.DAL;
using System.Data.Objects;
using System.Configuration;
using System.Xml;
using System.IO;
using MetaviewerDataLayer.DAL.EntityFramework;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace MetaviewerDataLayer.DAL
{
    public class DocumentPages
    {

        public DocumentPages()
        {

        }

        /// <summary>
        /// Will return a list of Page object given the required doc id excluding all
        /// documents where last route is 85
        /// </summary>
        /// <param name="docId"></param>
        /// <returns></returns>
        public List<Page> GetPagesById(int docId)
        {
            MetaviewerEntities context = new MetaviewerEntities();
            
            List<Page> pages = new List<Page>();

            try
            {
                pages.AddRange(context.Pages.Where(p => p.DocID == docId).ToList());
                pages = pages.OrderBy(pg => pg.PageNumber).ToList();
            }
            catch (Exception ex)
            {
                HandleSerror(ex);
            }

            return pages;
        }

        /// <summary>
        /// Will return a list of Page object excluding all documents where last route is 85
        /// </summary>
        /// <returns></returns>
        public List<Page> GetPages()
        {
            MetaviewerEntities context = new MetaviewerEntities();
            List<Page> pages = new List<Page>();
            List<Page> tempList = new List<Page>();

            try
            {
                context.Pages.ToList();
                pages.AddRange(tempList);
                pages = pages.OrderBy(pg => pg.PageNumber).ToList();
            }
            catch (Exception ex)
            {
                HandleSerror(ex);
            }

            return pages;
        }


        private static void HandleSerror(Exception ex)
        {
            string msg = ex.Message + "\n\n";
            if (ex.InnerException != null)
            {
                msg += ex.InnerException.Message + "\n\n";
                msg += ex.InnerException.StackTrace;
            }
            msg += ex.StackTrace;

            Logger.Write(msg);
        }


    }
}

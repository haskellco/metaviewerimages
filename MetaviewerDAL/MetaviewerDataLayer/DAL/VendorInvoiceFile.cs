﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaviewerDataLayer.DAL
{
    public class VendorInvoiceFile
    {
        private string vendorName;

        public string VendorName
        {
            get { return vendorName; }
            set { vendorName = value; }
        }
        private string invoiceNumber;

        public string InvoiceNumber
        {
            get { return invoiceNumber; }
            set { invoiceNumber = value; }
        }
        private string invoiceDate;

        public string InvoiceDate
        {
            get { return invoiceDate; }
            set { invoiceDate = value; }
        }

        private string sourceFilePath;

        public string SourceFilePath
        {
            get { return sourceFilePath; }
            set { sourceFilePath = value; }
        }

        private string fileName;

        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        public VendorInvoiceFile()
        {
        }
    }
}

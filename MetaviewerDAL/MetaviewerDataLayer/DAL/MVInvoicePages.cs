﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using MetaviewerDataLayer.DAL.EntityFramework;
using System.Data.Objects;
using System.Configuration;
using System.Xml;
using System.IO;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Data;

namespace MetaviewerDataLayer.DAL
{
    public class MVInvoicePages
    {
        public MetaviewerEntities context = new MetaviewerEntities();

        public MVInvoicePages()
        {

        }
        public List<int> GetVisaStatementIDs(string empNo, string stDate)
        {
            MetaviewerDataLayer.DAL.EntityFramework.MetaviewerEntities c = new MetaviewerEntities();

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["MVSQLConn"].ToString());
            List<int> result = new List<int>();

            string query = SQLStrings.GetVisaStatementsDocIDs;

            connection.Open();

            try
            {
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.Add(new SqlParameter("EmpNo", empNo));
                command.Parameters.Add(new SqlParameter("STDate", stDate));
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var val = (Int32)reader["DocID"];
                        result.Add(val);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                connection.Close();
            }

            return result;
        }

        public List<GetDocumentsPage_Result> GetInvoicePages(string E1DocNo, int vendorNo = 0)
        {
            MetaviewerDataLayer.DAL.EntityFramework.MetaviewerEntities c = new MetaviewerEntities();

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["MVSQLConn"].ToString());
            List<GetDocumentsPage_Result> result = new List<GetDocumentsPage_Result>();
            GetDocumentsPage_Result resultObj;

            string query = SQLStrings.GetInvoiceImages;
            if (vendorNo != 0) query = SQLStrings.GetInvoiceImagesByVendor;

            connection.Open();

            try
            {
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.Add(new SqlParameter("DocNo", E1DocNo));
                if (vendorNo != 0) command.Parameters.Add(new SqlParameter("Vendor", vendorNo));
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        resultObj = new GetDocumentsPage_Result();
                        resultObj.DocName = reader["DocName"] as String;
                        resultObj.DosPath = reader["DosPath"] as String;
                        resultObj.E1_DOC_no = reader["E1 DOC no"] as String;
                        resultObj.MagSubDir = reader["MagSubDir"] as String;
                        resultObj.PageNumber = (Int32)reader["PageNumber"];
                        resultObj.SysFile = reader["SysFile"] as String;
                        resultObj.TypeID = (Int32)reader["TypeID"];
                        resultObj.VendorNumber = (reader["Vendor Number"] != null) ? reader["Vendor Number"].ToString() : "0";

                        result.Add(resultObj);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                connection.Close();
            }

            return result;
        }


        public List<VendorInvoiceFile> GetInvoiceFiles(string likeJobNumber, DateTime fromDate, DateTime toDate, string tresholdAmt)
        {

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["MVSQLConn"].ToString());
            List<VendorInvoiceFile> result = new List<VendorInvoiceFile>();
            VendorInvoiceFile resultObj;
            string query = SQLStrings.GetInvoiceImagesByJobAndDate;
            //DateTime fDate = fromDate;
            //DateTime tDate = toDate;
            string fDate = fromDate.ToString();
            string tDate = toDate.ToString();
            decimal treshold = decimal.Parse(tresholdAmt);
            connection.Open();

            try
            {
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.Add(new SqlParameter("jobNumber", likeJobNumber));
                command.Parameters.Add(new SqlParameter("@fromDate", fDate));
                command.Parameters.Add(new SqlParameter("@toDate", tDate ));
                command.Parameters.Add(new SqlParameter("thresholdAmount", treshold));
                SqlDataReader reader = command.ExecuteReader();

                if(reader.HasRows)
                {
                    while (reader.Read())
                    {
                        resultObj = new VendorInvoiceFile();
                        resultObj.VendorName = reader["Vendor"] as String;
                        resultObj.InvoiceNumber = reader["Invoice"] as String;
                        DateTime date = Convert.ToDateTime(reader["Invoice Date"]);
                        resultObj.InvoiceDate = (date != null) ? String.Format("{0:yyyy}",date) + String.Format("{0:MM}",date) + String.Format("{0:dd}",date): "NODATE";
                        resultObj.SourceFilePath = reader["DosPath"] as String + @"\" + 
                                                   reader["MagSubDir"] as String + @"\" + 
                                                   reader["SysFile"] as String;
                        string fileNo = (reader["SysFile"] as String).Substring((reader["SysFile"] as String).LastIndexOf('.') - 2, 2);
                        resultObj.FileName = "V-" + ((resultObj.VendorName.Length > 20) ? resultObj.VendorName.Substring(0, 20) : resultObj.VendorName) + "-I-" +
                                             ((resultObj.InvoiceNumber.Length <= 10) ? resultObj.InvoiceNumber : resultObj.VendorName.Substring(0, 10)) + "-D-" + resultObj.InvoiceDate + "-" + fileNo;
                        result.Add(resultObj);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                connection.Close();
            }

            return result;
        }

        public List<VendorInvoiceFile> GetMetaviewerPages(string inDocNumbers)
        {

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["MVSQLConn"].ToString());
            List<VendorInvoiceFile> result = new List<VendorInvoiceFile>();
            VendorInvoiceFile resultObj;
            string query = SQLStrings.GetExpenseReportsByE1DocNoAndDate;

            connection.Open();

            try
            {
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.Add(new SqlParameter("@docNumbers", inDocNumbers));
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        resultObj = new VendorInvoiceFile();
                        resultObj.VendorName = reader["Vendor"] as String;
                        resultObj.InvoiceNumber = reader["Invoice"] as String;
                        DateTime date = Convert.ToDateTime(reader["Invoice Date"]);
                        resultObj.InvoiceDate = (date != null) ? String.Format("{0:yyyy}", date) + String.Format("{0:MM}", date) + String.Format("{0:dd}", date) : "NODATE";
                        resultObj.SourceFilePath = reader["DosPath"] as String + @"\" +
                                                   reader["MagSubDir"] as String + @"\" +
                                                   reader["SysFile"] as String;
                        string fileNo = (reader["SysFile"] as String);
                        resultObj.FileName = "V-" + ((resultObj.VendorName.Length > 20) ? resultObj.VendorName.Substring(0, 20) : resultObj.VendorName) + "-I-" +
                                             ((resultObj.InvoiceNumber.Length <= 10) ? resultObj.InvoiceNumber : resultObj.VendorName.Substring(0, 10)) + "-D-" + resultObj.InvoiceDate + fileNo;
                        result.Add(resultObj);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                connection.Close();
            }

                            return result;  
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetaviewerDataLayer.DAL.EntityFramework;
using System.Data.Objects;

namespace MetaviewerDataLayer
{
    class TestClass
    {
        public MetaviewerEntities context = new MetaviewerEntities();

        public TestClass(string E1DocNo)
        {
            ObjectResult<GetDocumentsPage_Result> result = context.GetDocumentsPage(E1DocNo);
            string[] filePaths = new string[result.Count()];

            if (result.Count() > 0)
            {
                int i = 0;
                foreach (var item in result)
                {
                    filePaths[i] = item.DosPath.Trim() + "\\" + item.MagSubDir.Trim() + "\\" + item.SysFile.Trim();
                    i++;
                }

                MergeTIFImageFiles(filePaths);
            }
            else
            {
                throw new Exception("No files found for E1 Document Number: " + E1DocNo);
            }

        }

        private void MergeTIFImageFiles(string[] filePaths)
        {
            throw new NotImplementedException();
        }
    }
}

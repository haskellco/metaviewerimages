﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using MetaviewerDataLayer.DAL.EntityFramework;
using System.Data.Objects;
using System.Reflection;
using System.Collections;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using MetaviewerDataLayer.DAL;

namespace MetaviewerImages
{
    public class VendorInvoices
    {
        #region Properties
        private string copyToPath;
        public string CopyToPath
        {
            get { return copyToPath; }
            set { copyToPath = value; }
        } 
        #endregion

        #region Constructors
        public VendorInvoices()
        {

        } 
        #endregion

        #region Public Methods  

        /// <summary>
        /// Will return a path to a multiple tiff or pdf file based on specified parameters.
        /// </summary>
        /// <param name="E1DocumentNumber"> Required document number tied to Enterprise One</param>
        /// <param name="NumberOfPages">Optional number of pages needed.  Max is 10</param>
        /// <param name="vendor">Optional vendor number</param>
        /// <param name="pdf">Optional flag to determine weather a pdf file is returned or a merged tiff file</param>
        /// <returns></returns>
        public string GetInvoiceImage(string E1DocumentNumber, int NumberOfPages = 10, int vendor = 0, bool pdf = true)
        {
            MVInvoicePages context = new MVInvoicePages();
            List<GetDocumentsPage_Result> tifImages = context.GetInvoicePages(E1DocumentNumber);
            ArrayList filePaths = new ArrayList();

            try
            {
                if (String.IsNullOrEmpty(CopyToPath))
                {
                    if (vendor != 0)
                    {
                        tifImages = context.GetInvoicePages(E1DocumentNumber, vendor);
                    }
                    else
                    {
                        tifImages = context.GetInvoicePages(E1DocumentNumber);
                    }

                    if (pdf)
                    {
                        CopyToPath = ConfigurationManager.AppSettings["InvoiceDirectory"].ToString() +
                            E1DocumentNumber + "_" + NumberOfPages + ".pdf";
                        if (vendor != 0)
                        {
                            CopyToPath = ConfigurationManager.AppSettings["InvoiceDirectory"].ToString() +
                            E1DocumentNumber + "_" + vendor.ToString() + "_" + NumberOfPages + ".pdf";
                        }
                    }
                    else
                    {
                        CopyToPath = ConfigurationManager.AppSettings["InvoiceDirectory"].ToString() +
                            E1DocumentNumber + "_" + NumberOfPages + ".tif";
                        if (vendor != 0)
                        {
                            CopyToPath = ConfigurationManager.AppSettings["InvoiceDirectory"].ToString() +
                            E1DocumentNumber + "_" + vendor.ToString() + "_" + NumberOfPages + ".tif";
                        }
                    }
                }

                if (tifImages.Count > 0)
                {
                    tifImages = tifImages.OrderBy(r => r.PageNumber).ToList();

                    // validate number of pages
                    if (NumberOfPages == 0) NumberOfPages = 10; // Default and cannot be 0

                    int processed = 0;
                    foreach (var item in tifImages)
                    {
                        filePaths.Add(item.DosPath.Trim() + "\\" + item.MagSubDir.Trim() + "\\" + item.SysFile.Trim());
                        processed++;

                        // check if reached max pages and if will need to let user know about it
                        if (processed == 10 && tifImages.Count > 10)
                        {
                            filePaths.Add(ConfigurationManager.AppSettings["UserNoticeTiff"].ToString());
                            processed++;
                        }
                        
                        if (processed == NumberOfPages || processed == 11) break;
                    }

                    if (pdf) MergeTIFF2Pdf(filePaths, CopyToPath);  //Default at this time
                    else MergeTIFImageFiles(filePaths, CopyToPath);
                }
                else
                {
                    throw new Exception("No files found for E1 Document Number: " + E1DocumentNumber);
                }
            }
            catch (Exception e)
            {
                if(e.Message.StartsWith("No files found")) CopyToPath = e.Message;
                else
                {
                    string msg = "Error on VendorInvoices class:\n" + e.Message + "\n\n" + e.StackTrace + "\n\n";
                    // Modified by G. Vera 03/24/2012 - Changed to throw exception so UI can catch it.
                    throw new Exception(msg);
                }
            }

            return CopyToPath;
        }

        #region Not in Use
        /// <summary>
        /// Returns the stream of the merged invoice image file given the number of pages.
        /// Pages are sorted in an ascending order.
        /// </summary>
        /// <param name="E1DocumentNumber"></param>
        /// <param name="NumberOfPages"></param>
        /// <param name="adressNo"></param>
        /// <returns></returns>
        //public Stream GetInvoiceImage(string E1DocumentNumber, string vendor = " ", int NumberOfPages = 10)
        //{
        //    MVInvoicePages context = new MVInvoicePages();
        //    List<GetDocumentsPage_Result> results = new List<GetDocumentsPage_Result>();
        //    ArrayList filePaths = new ArrayList();

        //    try
        //    {
        //        if (vendor.Trim() != "")
        //        {
        //            // TODO:  Implement a method in DAL that gets data with address number as well.
        //            // results = context.GetInvoicePages(E1DocumentNumber);
        //        }
        //        else
        //        {
        //            results = context.GetInvoicePages(E1DocumentNumber);
        //        }


        //        if (results.Count > 0)
        //        {
        //            results = results.OrderBy(r => r.PageNumber).ToList();

        //            // validate number of pages
        //            if (NumberOfPages == 0) NumberOfPages = 10; // Default and cannot be 0

        //            int counter = 0;
        //            foreach (var item in results)
        //            {
        //                filePaths.Add(item.DosPath.Trim() + "\\" + item.MagSubDir.Trim() + "\\" + item.SysFile.Trim());
        //                counter++;

        //                if (counter == 10 && results.Count > 10)
        //                {
        //                    // TODO:  Need to get that "user message" TIFF and append
        //                }

        //                // made max pages be 11 including the user message TIFF
        //                if (counter == NumberOfPages || counter == 12) break;
        //            }
        //        }
        //        else
        //        {
        //            throw new Exception("No files found for E1 Document Number: " + E1DocumentNumber);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Logger.Write("Error on VendorInvoices.cs class:\n" + e.Message + "\n\n" + e.StackTrace);
        //    }

        //    return MergeTIFImageFiles(filePaths);
        //}  
        #endregion

        #endregion

        #region Private Methods

        private void MergeTIFImageFiles(string[] filePaths, string copyTo)
        {
            TiffManager tManager = new TiffManager();
            tManager.JoinTiffImages(filePaths, copyTo, System.Drawing.Imaging.EncoderValue.CompressionCCITT4);
        }

        private void MergeTIFImageFiles(ArrayList filePaths, string copyTo)
        {
            TiffManager tManager = new TiffManager();
            tManager.JoinTiffImages(filePaths, copyTo, System.Drawing.Imaging.EncoderValue.CompressionCCITT4);
        }

        private string MergeTIFF2Pdf(ArrayList filePaths, string copyTo)
        {
            TiffManager tManager = new TiffManager();
            return tManager.JoinTiffImages2Pdf(filePaths, copyTo);
        }

        #region Not in Use
        /// <summary>
        /// This will retunr the Stream where Image file was saved to
        /// </summary>
        /// <param name="filePaths"></param>
        //private Stream MergeTIFImageFiles(ArrayList filePaths)
        //{
        //    TiffManager tManager = new TiffManager();
        //    return tManager.JoinTiffImages(filePaths, System.Drawing.Imaging.EncoderValue.CompressionCCITT4);
        //}  
        #endregion


        #endregion

    }
}

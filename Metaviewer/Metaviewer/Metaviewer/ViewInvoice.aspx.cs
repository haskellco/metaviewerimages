﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace TestTIFFViewer
{
    public partial class ViewInvoice : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Clear();

            System.Drawing.Image TheImg = new TIF(context.Request.QueryString["FilePath"]).GetTiffImageThumb(System.Convert.ToInt16(context.Request.QueryString["Pg"]), System.Convert.ToInt16(context.Request.QueryString["Height"]), System.Convert.ToInt16(context.Request.QueryString["Width"]));
            if (TheImg != null)
            {
                switch (context.Request.QueryString["Rotate"])
                {
                    case "90":
                        TheImg.RotateFlip(System.Drawing.RotateFlipType.Rotate90FlipNone);
                        break;
                    case "180":
                        TheImg.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
                        break;
                    case "270":
                        TheImg.RotateFlip(System.Drawing.RotateFlipType.Rotate270FlipNone);
                        break;
                }

                context.Response.ContentType = "image/png";
                TheImg.Save(context.Response.OutputStream, System.Drawing.Imaging.ImageFormat.Png);
                TheImg.Dispose();
            }
        }

        public bool IsReusable
        {
            get
            {
                // TODO:  Add SimpleHandler.IsReusable 
                // getter implementation
                return false;
            }
        }

    }
}
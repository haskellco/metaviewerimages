﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="Metaviewer.Main"
         MasterPageFile="~/ImageViewer.master" %>

<asp:Content ID="HedaerContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="ToolBarContent" runat="server" ContentPlaceHolderID="ToolBar">
    <asp:HyperLink runat="server" ID="_hlRot90" Text="Rotate Right" 
        Font-Underline="True" ForeColor="#022D57"/>&nbsp;
    <asp:HyperLink runat="server" ID="_hlRot270" Text="Rotate Left" Font-Underline="true" ForeColor="#022D57" />&nbsp;
    <asp:HyperLink runat="server" ID="_hlRot180" Text="Rotate 180" Font-Underline="true" ForeColor="#022D57"/>&nbsp;
    <asp:HyperLink runat="server" ID="_hlBig" Text="Bigger" Font-Underline="true" ForeColor="#022D57"/>&nbsp;
    <asp:HyperLink runat="server" ID="_hlSmall" Text="Revert" Font-Underline="true" ForeColor="#022D57"/>&nbsp;<br />
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div>
            <asp:Table runat="server" ID="_tblImgs" Height="100%" Width="100%" Font-Names="Calibri">
            <asp:TableRow HorizontalAlign="Center">
                <asp:TableCell Width="10%"  BorderColor="#999999" BorderStyle="Solid" BorderWidth="0" VerticalAlign="Top">
                    <asp:Panel runat="server" ScrollBars="Vertical" Visible="true" Height="500px">
                        <asp:Label runat="server" ID="_lblClickNote" Text="*Click to view bigger" Font-Size="Small"/>
                        <br /><br />
                        <asp:PlaceHolder runat="server" ID="_plcImgsThumbs" />
                        <br />        
                        <asp:PlaceHolder runat="server" ID="_plcImgsThumbsPager" />
                        <br />
                    </asp:Panel>     
                </asp:TableCell>
                
                <asp:TableCell Width="50%" BorderColor="#999999" BorderStyle="Solid" BorderWidth="0"
                                HorizontalAlign="Center" VerticalAlign="Top">
                    <asp:Panel ID="Panel1" runat="server" ScrollBars="Vertical" Visible="true" Height="500px">
                        <asp:PlaceHolder runat="server" ID="_plcBigImg" />
                        <br />
                    </asp:Panel>
                </asp:TableCell>

                </asp:TableRow></asp:Table>

   </div><script language="javascript" type="text/javascript">
            function ChangePg(Pg) {
                Src = 'ViewInvoice.ashx?View=1&FilePath=' + GetBigSrc("FilePath") + "&Pg=" + Pg + "&Height=" + GetBigSrc("Height") + "&Width=" + GetBigSrc("Width");
                SrcBig = 'ViewInvoice.ashx?View=1&FilePath=' + GetBigSrc("FilePath") + "&Pg=" + Pg + "&Height=" + 1000 + "&Width=" + 1000;
                SrcRevert = 'ViewInvoice.ashx?View=1&FilePath=' + GetBigSrc("FilePath") + "&Pg=" + Pg + "&Height=" + 600 + "&Width=" + 600;
                document.getElementById('MainContent__imgBig').src = Src;
                document.getElementById('ToolBar__hlRot270').onclick = function () { ChangePg(Pg); document.getElementById('MainContent__imgBig').src = Src + "&Rotate=270"; };
                document.getElementById('ToolBar__hlRot180').onclick = function () { ChangePg(Pg); document.getElementById('MainContent__imgBig').src = Src + "&Rotate=180"; };
                document.getElementById('ToolBar__hlRot90').onclick = function () { ChangePg(Pg); document.getElementById('MainContent__imgBig').src = Src + "&Rotate=90"; };
                document.getElementById('ToolBar__hlBig').onclick = function () { ChangePg(Pg); document.getElementById('MainContent__imgBig').src = SrcBig + "&Rotate=" + GetBigSrc("Rotate"); };
                document.getElementById('ToolBar__hlSmall').onclick = function () { ChangePg(Pg); document.getElementById('MainContent__imgBig').src = SrcRevert; };
            }

            function GetBigSrc(Qrystr) {
                var Qry = document.getElementById('MainContent__imgBig').src;
                //alert(Qry);
                gy = Qry.split("&");
                for (i = 0; i < gy.length; i++) {
                    ft = gy[i].split("=");
                    if (ft[0] == Qrystr)
                        return ft[1];
                }
            }      
    </script></asp:Content>
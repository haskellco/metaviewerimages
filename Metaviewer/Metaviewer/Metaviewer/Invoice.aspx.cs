﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MetaviewerImages;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace Metaviewer
{
    public partial class Invoice : System.Web.UI.Page
    {
        private string _filePath;

        protected void Page_Load(object sender, EventArgs e)
        {
            // get the parameter values needed
            string E1DocumentNumber = String.Empty;
            int vendor = 0;
            int maxPages = Convert.ToInt32(ConfigurationManager.AppSettings["MaxPages"]);    // Modified by G. Vera 12/05/2012 - Made the maximum pages to come from web.config.


            try
            {
                E1DocumentNumber = (this.Request.QueryString["Doc"] != null) ? this.Request.QueryString["Doc"].ToString() : " ";
                vendor = (this.Request.QueryString["Vend"] != null) ? Int32.Parse(this.Request.QueryString["Vend"]) : 0;
            }
            catch (Exception ex)
            {
                // Modified by G. Vera 03/24/2012 - Added document number to the error email
                Logger.Write("Document Number: " + E1DocumentNumber + "\n\n" + ex.Message + "\n\n" + ex.StackTrace);
                Response.Redirect("~/ErrorPage.aspx?m=Invalid document number or vendor number.");
            }

            // call the MetaviewerImages class library that processes the tiff images
            try
            {
                VendorInvoices invImg = new VendorInvoices();
                // Modified by G. Vera 12/05/2012 - Made the maximum pages to come from web.config.
                _filePath = invImg.GetInvoiceImage(E1DocumentNumber, maxPages, vendor);
            }
            catch (Exception ex)
            {
                // Modified by G. Vera 03/24/2012 - Added document number to the error email
                Logger.Write("Document Number: " + E1DocumentNumber + "\n\n" + ex.Message + "\n\n" + ex.StackTrace);
                Response.Redirect("~/ErrorPage.aspx?");
            }

            // validate and return
            if (_filePath != "")
            {
                if (!_filePath.StartsWith("No files found"))
                {
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.ContentType = "Application/pdf";
                    Response.WriteFile(_filePath);
                    Response.End();
                }
                else
                {
                    // NOTE:  Use the ';' character to determine where the message needs a carraige return
                    Response.Redirect("~/ErrorPage.aspx?m=" + "Invoice Image not found.;;" + 
                                                              "The requested document cannot be retrieved.;" +
                                                              "Please use the Metaviewer document imaging system or " + 
                                                              "contact Accounts Payable for more information on this " + 
                                                              "invoice.");
                }
            }
            else
            {
                Response.Redirect("~/ErrorPage.aspx?");
            }


            #region Test error email only
            //try
            //{
            //    throw new Exception("This is a test Exception");

            //    // Get the merged TIFF and save it in application's directory
            //    VendorInvoices invImg = new VendorInvoices();
            //    FilePath = invImg.GetInvoiceImage(E1DocumentNumber, 10, vendor);

            //    if (FilePath != "")
            //    {
            //        Response.Clear();
            //        Response.ContentType = "Application/pdf";
            //        Response.WriteFile(FilePath);
            //        Response.End();
            //    }
            //    else
            //    {
            //        Response.Redirect("~/ErrorPage.aspx");
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Logger.Write("Exception message:\n" + ex.Message + "\n\nStack Trace:\n" + ex.StackTrace);
            //    Response.Redirect("~/ErrorPage.aspx");
            //}
            #endregion

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Metaviewer
{
    public partial class ErrorPage : System.Web.UI.Page
    {
        private const string ERROR_MESSAGE = "An error occured while processing the request.<br /><br />An email has been sent to helpdesk.  You should be contacted shortly about this issue.";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["m"] != null)
            {
                string msgWithBreaks = String.Empty;

                if (Request.QueryString["m"].Contains(";"))
                {
                    msgWithBreaks = Request.QueryString["m"].Replace(";", @"<br />");
                    MainMessage.InnerHtml = msgWithBreaks;
                }
                else MainMessage.InnerHtml = Request.QueryString["m"];
            }
            else
            {
                MainMessage.InnerHtml = ERROR_MESSAGE;
            }

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Metaviewer;
using MetaviewerImages;
using System.IO;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace Metaviewer
{
    public partial class Main : System.Web.UI.Page
    {
        int DefaultThumbHieght = 100;
        int DefaultThumbWidth = 100;
        int DefaultBigHieght = 600;
        int DefaultBigWidth = 600;
        int PagerSize = 8;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Set it so page doesn't cache
            System.Web.UI.HtmlControls.HtmlMeta META = new System.Web.UI.HtmlControls.HtmlMeta();
            META.HttpEquiv = "Pragma";
            META.Content = "no-cache";
            Page.Header.Controls.Add(META);
            Response.Expires = -1;
            Response.CacheControl = "no-cache";

            try
            {

                //throw new Exception("This is only a Test Error");

                string E1DocumentNumber = (this.Request.QueryString["Doc"] != null) ? this.Request.QueryString["Doc"].ToString() : " ";
                int vendor = (this.Request.QueryString["Vend"] != null) ? Int32.Parse(this.Request.QueryString["Vend"]) : 0;

                // Get the merged TIFF and save it in application's directory
                VendorInvoices invImg = new VendorInvoices();

                #region TODO: Streaming
                //System.Drawing.Image mergedTiff = System.Drawing.Image.FromStream(invImg.GetInvoiceImage("1025148", 10, " "));
                //string a = Directory.GetCurrentDirectory();
                //string savePath = a.ToString() + "\\" + E1DocumentNumber + "_10" + ".tif";
                //mergedTiff.Save(savePath, System.Drawing.Imaging.ImageFormat.Tiff); 
                #endregion

                #region TODO
                // TODO:  need to find a way to send a stream object to the http handler that processes
                // the thumbnails.
                //mergedTiff.Save(this.Response.OutputStream, System.Drawing.Imaging.ImageFormat.Tiff);
                #endregion

                FilePath = invImg.GetInvoiceImage(E1DocumentNumber, 10, vendor);

                if (FilePath != "")
                {
                    //Determine Start/End Pages
                    int StartPg = 1;
                    if (Request.QueryString["StartPage"] != null)
                        StartPg = System.Convert.ToInt16(Request.QueryString["StartPage"]);
                    int BigImgPg = StartPg;
                    int EndPg = StartPg + (PagerSize - 1);
                    if (EndPg > TotalTIFPgs)
                        EndPg = TotalTIFPgs;

                    //Add/configure the thumbnails
                    while (StartPg <= EndPg)
                    {
                        //Label lbl = new Label();
                        //if (StartPg % 4 == 0 && StartPg != 0) lbl.Text = "<br />";
                        //else lbl.Text = "&nbsp;";

                        Image Img = new Image();
                        Img.BorderColor = System.Drawing.Color.LightGray;
                        Img.BorderStyle = (BorderStyle)Enum.Parse(typeof(BorderStyle), "Solid");
                        Img.BorderWidth = Unit.Parse("1");
                        Img.Attributes.Add("onClick", "ChangePg(" + StartPg.ToString() + ");");
                        Img.Attributes.Add("onmouseover", "this.style.cursor='hand';");
                        Img.ImageUrl = "ViewInvoice.ashx?FilePath=" + FilePath + "&Pg=" + (StartPg).ToString() + "&Height=" + DefaultThumbHieght.ToString() + "&Width=" + DefaultThumbWidth;
                        _plcImgsThumbs.Controls.Add(Img);
                        //_plcImgsThumbs.Controls.Add(lbl);
                        StartPg++;
                    }

                    //Bind big img
                    Image BigImg = new Image();
                    BigImg.BorderStyle = (BorderStyle)Enum.Parse(typeof(BorderStyle), "Solid");
                    BigImg.BorderWidth = Unit.Parse("1");
                    BigImg.ID = "_imgBig";
                    BigImg.ImageUrl = "ViewInvoice.ashx?View=1&FilePath=" + FilePath + "&Pg=" + BigImgPg.ToString() + "&Height=" + DefaultBigHieght.ToString() + "&Width=" + DefaultBigWidth.ToString();
                    _plcBigImg.Controls.Add(BigImg);

                    //Config actions
                    _hlRot90.Attributes.Add("OnClick", "document.getElementById('_imgBig').src = 'ViewInvoice.ashx?View=1&FilePath=" + FilePath.ToString().Replace(@"\", "%5C") + "&Pg=" + BigImgPg.ToString() + "&Height=" + DefaultBigHieght.ToString() + "&Width=" + DefaultBigWidth.ToString() + "&Rotate=90';");
                    _hlRot90.Attributes.Add("onmouseover", "this.style.cursor='hand';");

                    _hlRot180.Attributes.Add("OnClick", "document.getElementById('_imgBig').src = 'ViewInvoice.ashx?View=1&FilePath=" + FilePath.ToString().Replace(@"\", "%5C") + "&Pg=" + BigImgPg.ToString() + "&Height=" + DefaultBigHieght.ToString() + "&Width=" + DefaultBigWidth.ToString() + "&Rotate=180';");
                    _hlRot180.Attributes.Add("onmouseover", "this.style.cursor='hand';");

                    _hlRot270.Attributes.Add("OnClick", "document.getElementById('_imgBig').src = 'ViewInvoice.ashx?View=1&FilePath=" + FilePath.ToString().Replace(@"\", "%5C") + "&Pg=" + BigImgPg.ToString() + "&Height=" + DefaultBigHieght.ToString() + "&Width=" + DefaultBigWidth.ToString() + "&Rotate=270';");
                    _hlRot270.Attributes.Add("onmouseover", "this.style.cursor='hand';");

                    _hlBig.Attributes.Add("OnClick", "document.getElementById('_imgBig').src = 'ViewInvoice.ashx?View=1&FilePath=" + FilePath.ToString().Replace(@"\", "%5C") + "&Pg=" + BigImgPg.ToString() + "&Height=" + (DefaultBigHieght + 400).ToString() + "&Width=" + (DefaultBigWidth + 400).ToString() + "';");
                    _hlBig.Attributes.Add("onmouseover", "this.style.cursor='hand';");

                    _hlSmall.Attributes.Add("OnClick", "document.getElementById('_imgBig').src = 'ViewInvoice.ashx?View=1&FilePath=" + FilePath.ToString().Replace(@"\", "%5C") + "&Pg=" + BigImgPg.ToString() + "&Height=" + (DefaultBigHieght).ToString() + "&Width=" + (DefaultBigWidth).ToString() + "';");
                    _hlSmall.Attributes.Add("onmouseover", "this.style.cursor='hand';");

                    #region Pagers not Needed as of now (bug fix needed)
                    //ConfigPagers
                    //Config page 1 - whatever
                    //if ((TotalTIFPgs / PagerSize) >= 1)
                    //{
                    //    HyperLink _hl = new HyperLink();
                    //    Label lbl = new Label(); lbl.Text = "&nbsp;";
                    //    if (Request.Url.ToString().IndexOf("&StartPage=") >= 0)
                    //        _hl.NavigateUrl = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("&StartPage=")) + "&StartPage=1";
                    //    else
                    //        _hl.NavigateUrl = Request.Url.ToString() + "&StartPage=1";
                    //    if ((1 + PagerSize) > TotalTIFPgs)
                    //        _hl.Text = "1-" + TotalTIFPgs;
                    //    else
                    //        _hl.Text = "1-" + PagerSize;
                    //    _plcImgsThumbsPager.Controls.Add(_hl);
                    //    _plcImgsThumbsPager.Controls.Add(lbl);
                    //}
                    ////Config the rest of the page pagers
                    //for (int i = 1; i <= (TotalTIFPgs / PagerSize); i++)
                    //{
                    //    HyperLink _hl = new HyperLink();
                    //    Label lbl1 = new Label(); lbl1.Text = "&nbsp;";
                    //    if (Request.Url.ToString().IndexOf("&StartPage=") >= 0)
                    //        _hl.NavigateUrl = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("&StartPage=")) + "&StartPage=" + ((i * PagerSize) + 1).ToString();
                    //    else
                    //        _hl.NavigateUrl = Request.Url.ToString() + "&StartPage=" + ((i * PagerSize) + 1).ToString();
                    //    if (i == (TotalTIFPgs / PagerSize))
                    //        _hl.Text = ((i * PagerSize) + 1).ToString() + "-" + TotalTIFPgs;
                    //    else
                    //        _hl.Text = ((i * PagerSize) + 1).ToString() + "-" + (((i + 1) * PagerSize)).ToString();
                    //    _plcImgsThumbsPager.Controls.Add(_hl);
                    //    _plcImgsThumbsPager.Controls.Add(lbl1);
                    //} 
                    #endregion
                }
                else
                {
                    Response.Write("Please provde a file path");
                }
            }
            catch (Exception ex)
            {
                Logger.Write("Error in the Main.aspx page\n\nMessage:  " + ex.Message + "\n\n" +
                             ex.StackTrace);

                Response.Redirect("~/ErrorPage.aspx?");
            }
        }

        public int TotalTIFPgs
        {
            get
            {
                if (ViewState["TotalTIFPgs"] == null)
                {
                    TIF TheFile = new TIF(FilePath);
                    ViewState["TotalTIFPgs"] = TheFile.PageCount;
                    TheFile.Dispose();
                }
                return System.Convert.ToInt16(ViewState["TotalTIFPgs"]);
            }
            set
            {
                ViewState["TotalTIFPgs"] = value;
            }
        }

        public String FilePath
        {
            get
            {
                if (ViewState["FilePath"] == null)
                {
                    return "";
                }
                return ViewState["FilePath"].ToString();
            }
            set
            {
                ViewState["FilePath"] = value;
            }

        }
    }
}
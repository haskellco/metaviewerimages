﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MetaviewerImages;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using MetaviewerDataLayer.DAL;

namespace Metaviewer
{
    public partial class document : System.Web.UI.Page
    {
        private static string _filePath;

        protected void Page_Load(object sender, EventArgs e)
        {
            // get the parameter values needed
            int docId = 0;
            string empNo = string.Empty;
            string statementDate = string.Empty;
            int maxPages = Convert.ToInt32(ConfigurationManager.AppSettings["MaxPages"]);    // Modified by G. Vera 12/05/2012 - Made the maximum pages to come from web.config.

            try
            {
                if (this.Request.QueryString["id"] != null)
                {
                    docId = int.Parse(this.Request.QueryString["id"]);
                }
                else if(this.Request.QueryString["emp"] != null)
                {
                    empNo = this.Request.QueryString["emp"];
                    statementDate = this.Request.QueryString["dt"];
                }
                else
                {
                    docId = 0;
                }

                // Testing Visa Statements
                //empNo = "140937";
                //statementDate = "2023-04-30";
                
            }
            catch (Exception ex)
            {
                Logger.Write(ex.Message + "\n\n" + ex.StackTrace);
                Response.Redirect("~/ErrorPage.aspx?m=Invalid document id");
            }

            // will call the MetaviewerImages class library that processes the tiff images
            MetaviewerImage blImage = new MetaviewerImage();

            // Modified by G. Vera 03/24/2012 - Added try/catch
            try
            {
                // Modified by G. Vera 03/24/2012 - Added document id to the error email
                // Modified by G. Vera 12/05/2012 - Made the maximum pages to come from web.config.
                if (docId > 0) _filePath = blImage.GetImage(docId, maxPages, true);
                else
                {
                    if (String.IsNullOrEmpty(empNo) || String.IsNullOrEmpty(statementDate))
                    {
                        Response.Redirect("~/ErrorPage.aspx?m=Invalid document id: " + docId.ToString());
                    }
                    else
                    {
                        // we have a request for Visa Statements
                        MVInvoicePages ip = new MVInvoicePages();
                        var docIDArr = ip.GetVisaStatementIDs(empNo, statementDate);
                        _filePath = blImage.GetImage(docIDArr);
                    }
                }
                    
                    
            }
            catch (Exception ex)
            {
                Logger.Write("Doc Id: " + docId + "\n\n" + ex.Message + "\n\n" + ex.StackTrace);
                Response.Redirect("~/ErrorPage.aspx");  // Generic error message
            }

            // validate and return
            if (_filePath != "")
            {
                if (!_filePath.Contains("not found") && !_filePath.Contains("Please"))
                {
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.ContentType = "Application/pdf";
                    Response.WriteFile(_filePath);
                    Response.End();
                }
                else if (_filePath.Contains("not found"))
                {
                    // NOTE:  Use the ';' character to determine where the message needs a carraige return
                    Response.Redirect("~/ErrorPage.aspx?m=" + "Document not found.;;" + 
                                                              "The requested document cannot be retrieved.;" +
                                                              "Please use the Metaviewer document imaging system or " + 
                                                              "contact Accounts Payable for more information on this " + 
                                                              "file.");
                }
                else
                {
                    Response.Redirect("~/ErrorPage.aspx?m=" + _filePath);
                }
            }
            else
            {
                Response.Redirect("~/ErrorPage.aspx");  // Generic error message
            }


            #region Test error email only
            //try
            //{
            //    throw new Exception("This is a test Exception");

            //    // Get the merged TIFF and save it in application's directory
            //    VendorInvoices invImg = new VendorInvoices();
            //    FilePath = invImg.GetInvoiceImage(E1DocumentNumber, 10, vendor);

            //    if (FilePath != "")
            //    {
            //        Response.Clear();
            //        Response.ContentType = "Application/pdf";
            //        Response.WriteFile(FilePath);
            //        Response.End();
            //    }
            //    else
            //    {
            //        Response.Redirect("~/ErrorPage.aspx");
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Logger.Write("Exception message:\n" + ex.Message + "\n\nStack Trace:\n" + ex.StackTrace);
            //    Response.Redirect("~/ErrorPage.aspx");
            //}
            #endregion

        }
    }
}
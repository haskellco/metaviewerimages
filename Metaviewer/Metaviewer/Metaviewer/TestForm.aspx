﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestForm.aspx.cs" Inherits="Metaviewer.TestForm"
         MasterPageFile="~/ImageViewer.master" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <table>
    <tr>
        <td>
            <asp:GridView ID="GridView1" runat="server" CellPadding="4" ForeColor="#333333" 
                GridLines="None">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <%--<asp:HyperLinkField DataNavigateUrlFields="E1_DOC_no" 
                        DataNavigateUrlFormatString="~\invoice.aspx?doc={0}&vend=0" DataTextField="E1_DOC_no" 
                        HeaderText="Document Number" />--%>
                    <asp:HyperLinkField DataNavigateUrlFields="DocId" DataNavigateUrlFormatString="~\document.aspx?id={0}"
                        DataTextField="DocId" HeaderText="Document ID" />
                </Columns>
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
            </asp:GridView>
            
        </td>
    </tr>
    <tr><td></td></tr>
    <tr><td></td></tr>
    </table>
</asp:Content>
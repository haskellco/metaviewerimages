﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Collections;

namespace Metaviewer
{
    public class MyHttpHandler :IHttpHandler
    {
        public bool IsReusable
        {
            get { return false; }
        }

        private bool _useStream = false;

        public void ProcessRequest(HttpContext context)
        {
            #region TODO
            //if(!_useStream)
            //    TheImg = new TIF(context.Request.QueryString["FilePath"]).GetTiffImageThumb(System.Convert.ToInt16(context.Request.QueryString["Pg"]), System.Convert.ToInt16(context.Request.QueryString["Height"]), System.Convert.ToInt16(context.Request.QueryString["Width"]));
            //else
            //    TheImg = new TIF().GetTiffImageThumb(s, System.Convert.ToInt16(context.Request.QueryString["Pg"]), System.Convert.ToInt16(context.Request.QueryString["Height"]), System.Convert.ToInt16(context.Request.QueryString["Width"])); 
            #endregion

            context.Response.Clear();
            System.Drawing.Image TheImg;

            TheImg = new TIF(context.Request.QueryString["FilePath"]).GetTiffImageThumb(System.Convert.ToInt16(context.Request.QueryString["Pg"]), 
                                                                      System.Convert.ToInt16(context.Request.QueryString["Height"]),
                                                                      System.Convert.ToInt16(context.Request.QueryString["Width"]));

            if (TheImg != null)
            {
                switch (context.Request.QueryString["Rotate"])
                {
                    case "90":
                        TheImg.RotateFlip(System.Drawing.RotateFlipType.Rotate90FlipNone);
                        break;
                    case "180":
                        TheImg.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
                        break;   
                    case "270":
                        TheImg.RotateFlip(System.Drawing.RotateFlipType.Rotate270FlipNone);
                        break;
                }

                context.Response.ContentType = "image/png";
                TheImg.Save(context.Response.OutputStream, System.Drawing.Imaging.ImageFormat.Png);
                TheImg.Dispose();
            }
        }
    }
}